import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class IncorrectLoginWrongEmailDataSourceTest extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTestDS(String wrongEmail) {

        new LoginPage(driver)
                .typeEmail(wrongEmail)
                .submitLoginWithFailure()
                .assertEmailErrorIsShown();
    }
}
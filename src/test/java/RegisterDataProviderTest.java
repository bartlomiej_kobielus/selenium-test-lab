import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class RegisterDataProviderTest extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectRegisterDP(String wrongEmail) {

        new LoginPage(driver)
                .goToRegisterPage()
                .typeEmail(wrongEmail)
                .registerWithFailure()
                .assertEmailErrorIsShown();
    }
}

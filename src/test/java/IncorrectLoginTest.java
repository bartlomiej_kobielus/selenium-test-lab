import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class IncorrectLoginTest extends SeleniumBaseTest {

    @Test
    public void IncorrectLoginTestPOP() {

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLoginWithFailure()
                .assertLoginErrorIsShown();
    }
}
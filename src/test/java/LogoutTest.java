import org.testng.annotations.Test;
import pages.LoginPage;

public class LogoutTest extends SeleniumBaseTest {

    @Test
    public void logoutTest() {

        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .logout()
                .assertUserSuccessfullyLogOut();

    }
}

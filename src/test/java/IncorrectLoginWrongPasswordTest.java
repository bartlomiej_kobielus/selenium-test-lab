import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class IncorrectLoginWrongPasswordTest extends SeleniumBaseTest {

    @Test
    public void incorrectLoginTestWrongPassword() {

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLoginWithFailure()
                .assertPasswordErrorListIsShown();
    }
}
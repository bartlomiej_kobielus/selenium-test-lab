import org.testng.annotations.Test;
import pages.LoginPage;

public class MenuTest extends SeleniumBaseTest {

    @Test
    public void testMenu() {

        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                     .assertProcessesHeader()
                     .assertProcessesUrl("http://localhost:4444/Projects")
                .goToCharacteristics()
                    .assertCharacteristicsUrl("http://localhost:4444/Characteristics")
                    .assertCharacteristicsHeader()
                .goToDashboard()
                    .assertDashboardUrl("http://localhost:4444/")
                    .assertDemoProjectIsShown();
    }
}

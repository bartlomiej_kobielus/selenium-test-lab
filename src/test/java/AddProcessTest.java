import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class AddProcessTest extends SeleniumBaseTest {

    @Test
    public void addProcessTest() {

        String processName = UUID.randomUUID().toString().substring(0, 10);

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .submitCreate()
                .assertProcessIsShown(processName);
    }
}

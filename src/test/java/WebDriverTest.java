import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class WebDriverTest extends SeleniumBaseTest {

    @Test
    public void playWithWebDriver() {

        WebElement searchInput = driver.findElement(By.cssSelector("input[name=q]"));
        searchInput.sendKeys("Quality Assurance");
        WebElement searchBtn = driver.findElement(By.cssSelector("input[value='Google Search']"));
        searchBtn.click();
    }
}

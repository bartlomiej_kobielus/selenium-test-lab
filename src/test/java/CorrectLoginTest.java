import org.testng.annotations.Test;
import pages.LoginPage;

public class CorrectLoginTest extends SeleniumBaseTest {


    @Test
    public void CorrectLoginTestPOP() {

        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .assertWelcomeElementIsShown();
    }
}
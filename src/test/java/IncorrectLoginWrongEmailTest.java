import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class IncorrectLoginWrongEmailTest extends SeleniumBaseTest {

    @Test
    public void incorrectLoginTestWrongEmail() {

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLoginWithFailure()
                .assertEmailErrorIsShown()
                .assertEmailErrorListIsShown();
    }
}

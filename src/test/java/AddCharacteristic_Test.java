import org.testng.annotations.Test;
import pages.LoginPage;
import java.util.UUID;

public class AddCharacteristic_Test extends SeleniumBaseTest {

    @Test
    public void addCharacteristic(){
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "10";

        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristic()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLsl(lsl)
                .typeUsl(usl)
                .submitCreate()
                .assertCharacteristic(characteristicName, lsl, usl, "");
    }
}

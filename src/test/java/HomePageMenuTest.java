import org.testng.annotations.Test;
import pages.LoginPage;

public class HomePageMenuTest extends SeleniumBaseTest {

    @Test
    public void gotToProcessesTest() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .goToCharacteristics()
                .goToDashboard();
    }
}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import java.util.List;
public class LoginPage {

    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".flash-message>strong")
    private WebElement logoutMsg;
    @FindBy(id = "Email")
    private WebElement emailTxt;
    @FindBy(id = "Email-error")
    public WebElement emailError;
    @FindBy(css = "#Password")
    private WebElement passwordTxt;
    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;
    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> loginErrors;
    @FindBy(css = "a[href*=Register]")
    private WebElement registerLnk;


    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);
    }

    public LoginPage submitLoginWithFailure() {
        loginBtn.click();
        return this;
    }

    public CreateAccountPage goToRegisterPage() {
        registerLnk.click();
        return new CreateAccountPage(driver);
    }

    public LoginPage assertLoginErrorIsShown() {
        Assert.assertEquals(loginErrors.get(0).getText(), "Invalid login attempt.");
        return this;
    }

    public LoginPage assertEmailErrorIsShown() {
        Assert.assertEquals(emailError.getText(), "The Email field is not a valid e-mail address.");
        return this;
    }

    public LoginPage assertPasswordErrorListIsShown() {

        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));

        boolean doesErrorExists = false;

        for (int i = 0; i < validationErrors.size(); i++)
            if (validationErrors.get(i).getText().equals("Invalid login attempt.")) {
                doesErrorExists = true;
                break;
            }
        Assert.assertTrue(doesErrorExists);
        return this;
    }

    public LoginPage assertEmailErrorListIsShown() {

        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));

        boolean doesErrorExists = false;

        for (int i = 0; i < validationErrors.size(); i++)
            if (validationErrors.get(i).getText().equals("The Email field is not a valid e-mail address.")) {
                doesErrorExists = true;
                break;
            }
        Assert.assertTrue(doesErrorExists);
        return this;
    }
    public LoginPage assertUserSuccessfullyLogOut() {
        Assert.assertTrue(logoutMsg.isDisplayed());
        Assert.assertEquals(logoutMsg.getText(), "User succesfully logged out");

        return this;
    }
}

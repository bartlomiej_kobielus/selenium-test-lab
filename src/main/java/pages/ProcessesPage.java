package pages;

import config.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import java.util.List;

public class ProcessesPage extends HomePage {

    private String GENERIC_PROCESS_XPATH = "//tbody//td[text()='%s']";
    private String PAGE_URL = new Config().getApplicationUrl() + "Projects";

    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(linkText = "Add new process")
    private WebElement addProcessBtn;
    @FindBy(css = ".page-title h3")
    private WebElement pageHeader;

    public CreateProcessPage clickAddProcess() {
        addProcessBtn.click();
        return new CreateProcessPage(driver);
    }

    public ProcessesPage assertProcessesHeader() {
        Assert.assertEquals(pageHeader.getText(), "Processes");
        return this;
    }

    public ProcessesPage assertProcessesUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);
        return this;
    }

    public ProcessesPage assertProcessIsShown(String processName) {
        String processXpath = String.format(GENERIC_PROCESS_XPATH, processName);
        WebElement process = driver.findElement(By.xpath(processXpath));
        Assert.assertTrue(process.isDisplayed());
        return this;
    }

    public ProcessesPage assertProcessIsNotShown(String processName) {
        String processXpath = String.format(GENERIC_PROCESS_XPATH, processName);
        List<WebElement> process = driver.findElements(By.xpath(processXpath));
        Assert.assertEquals(process.size(), 0);

        return this;
    }
}
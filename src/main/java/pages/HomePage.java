package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage {

    protected WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeElm;
    @FindBy(css = ".menu-workspace")
    private WebElement workspaceNav;
    @FindBy(css = ".menu-home")
    private WebElement homeNav;
    @FindBy(linkText = "Dashboard")
    private WebElement dashboardMenu;
    @FindBy(css = "a[href$=Characteristics]")
    private WebElement characteristicsMenu;
    @FindBy(css = "a[href$=Projects]")
    private WebElement processesMenu;
    @FindBy(css = "a[href*=Logout]")
    private WebElement logoutLnk;
    @FindBy(css = "a.user-profile")
    private WebElement userProfile;



    public HomePage assertWelcomeElementIsShown() {
        Assert.assertTrue(welcomeElm.getText().contains("Welcome"));
        return this;
    }

    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));
        return parent.getAttribute("class").contains("active");
    }

    public ProcessesPage goToProcesses() {
        if (!isParentExpanded(workspaceNav))
            workspaceNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processesMenu));

        processesMenu.click();
        return new ProcessesPage(driver);
    }

    public CharacteristicsPage goToCharacteristics() {
        if (!isParentExpanded(workspaceNav))
            workspaceNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processesMenu));

        characteristicsMenu.click();
        return new CharacteristicsPage(driver);
    }

    public DashboardPage goToDashboard() {
        if (!isParentExpanded(homeNav))
            homeNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(dashboardMenu));

        dashboardMenu.click();

        return new DashboardPage(driver);
    }
    public LoginPage logout() {
        userProfile.click();
        logoutLnk.click();

        return new LoginPage(driver);
    }
}